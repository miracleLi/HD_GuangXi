package com.hengda.smart.gxkjg.ui.common;

import android.app.Activity;
import android.os.Bundle;

import com.hengda.frame.update.UpdateService;
import com.hengda.frame.update.Utils.UpdaterUtil;
import com.hengda.smart.gxkjg.app.HdConstants;

public class UpdateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }
    public void checkUpdata() {
        UpdateService.Builder.create(HdConstants.APP_KEY,
                HdConstants.APP_SECRET,
                "1",
                UpdaterUtil.getVersionCode(this),
                UpdaterUtil.getDeviceId(this))
                .build(this);
    }
}
