package com.hengda.smart.gxkjg.ui.common;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.hengda.smart.gxkjg.tool.AndroidWorkaround;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;


/**
 * 作者：Tailyou
 * 时间：2016/1/18 08:50
 * 邮箱：tailyou@163.com
 * 描述：
 */
public class BaseActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

   public String permissions[] = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA};
    public boolean permissionGranted = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
    }

    protected void openActivity(Context context, Class<?> pClass) {
        openActivity(context, pClass, null, null);
    }

    protected void openActivity(Context context, Class<?> pClass, Bundle pBundle) {
        openActivity(context, pClass, pBundle, null);
    }

    protected void openActivity(Context context, Class<?> pClass, String action) {
        openActivity(context, pClass, null, action);
    }

    protected void openActivity(Context context, Class<?> pClass, Bundle pBundle, String action) {
        Intent intent = new Intent(context, pClass);
        if (action != null) {
            intent.setAction(action);
        }
        if (pBundle != null) {
            intent.putExtras(pBundle);
        }
        startActivity(intent);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (perms.size() == permissions.length) {
            permissionGranted = true;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        permissionGranted = false;
        if (permissionGranted){

            new AlertDialog.Builder(this).setMessage("授权失败，请在设置中打开应用所需权限")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    public void requestPermission(int requestCode, String[] perms, String tips) {
        if (EasyPermissions.hasPermissions(this, perms)) {
            if (perms.length == permissions.length) {
                permissionGranted=true;
            }
        } else {
            EasyPermissions.requestPermissions(this, tips, requestCode, perms);
        }
    }
}
